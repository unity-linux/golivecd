package main

/*
LiveCD iso build script
Copyright (C) 2019, Jeremiah Summers  <jmiahman@unity-linux.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import (
	"io/ioutil"
	"log"
	"os/exec"
	"fmt"
	"io"
	"os"
	"flag"
	"bytes"
	"sync"
	"strings"
	"strconv"
	"text/template"
)
// Global Stuff
var Latest_Kern string
var SE_Stat string
var Root_Dir string
var Tmp_Dir string
var Source_Dir string
var Live_Dir string
var Dist_Name string
var ISO_Name string
var ISO_Dest string
var Tmp_Loc string
var Live_User string
var Excludes string
var No_Clean bool
var Force_SELinux bool

// Usage functions
var Usage = func() {
  fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
  fmt.Printf(`
  --distname string        Name of the Distribution (used in bootmenus) (default "GOLiveCD")
  --isodest string         Destination of the ISO Generated (default "/tmp")
  --isoname string         Name of the ISO Generated (default "golivecd")
  --kver string            Kernel Version
  --noclean                Keep all files used for build
  --rootdir string         Root Dir Location (default "/")
  --srcdir string          Source Dir Location
  --tmploc string          Temp Dir Location (default "/tmp")
  --exclude string         Exclude files or directories (default "proc sys tmp mnt")

  `)
}

func user_check() {
				cmd := exec.Command("id", "-u")
				output, err := cmd.Output()

				if err != nil {
								log.Fatal(err)
				}

				// 0 = root, 501 = non-root user
				i, err := strconv.Atoi(string(output[:len(output)-1]))

				if err != nil {
					 log.Fatal(err)
				}

				if i != 0 {
					 fmt.Println("golivecd must be run as root! (sudo)")
				   log.Fatal("golivecd must be run as root! (sudo)")
				}
}


func selinux_stat() string {
   selinux_command := string("getenforce|sed -e 's| ||g'")
   var SE_Stat = run_shell(selinux_command)
         return SE_Stat
}

func Latest_KVer() string {
   kern_command := string("ls -1r "+Source_Dir+"/lib/modules/|sort -rV|head -n1|sed -e 's|/||g;s| ||g'")
   var Latest_Kern = run_shell(kern_command)
	 return Latest_Kern
}

// Parse bootloader template
func parse_boot_cfg(path string) {
	t, err := template.ParseFiles(path)
	if err != nil {
		log.Print(err)
		return
	}

	f, err := os.Create(path)
	if err != nil {
		log.Println("create file: ", err)
		return
	}

	// A sample config
	var disable_selinux string
	var restorecon_livesys string
        if strings.Contains(SE_Stat, "Disabled") {
           disable_selinux = string("selinux=0")
	   restorecon_livesys = string("")
        } else {
           disable_selinux = string("")
	   restorecon_livesys = string("/usr/sbin/restorecon -r /etc")
        }
	boot_time := string("100")
	boot_background := string("splash.jpg")
	unused_var := string("")
	config := map[string]string{
		"timeout":      boot_time,
		"boot_background": boot_background,
		"iso_name": ISO_Name,
		"dist_name": Dist_Name,
		"dracut_filesystems": "vfat msdos isofs xfs btrfs",
		"dracut_drivers": "squashfs",
		"live_user": Live_User,
                "disable_se": disable_selinux,
		"livesys_rest": restorecon_livesys,
		"unused_test": unused_var,
	}

	err = t.Execute(f, config)
	if err != nil {
		log.Print("execute: ", err)
		return
	}
	f.Close()
}

func start_logging() {
log.SetPrefix("Log: ")
log.SetFlags(log.Ldate | log.Lmicroseconds | log.Llongfile)
log.Println("init started")
logfile, err := os.OpenFile(Tmp_Dir+"/golivecd.log", os.O_CREATE|os.O_APPEND, 0644)
if err != nil {
		log.Fatal(err)
}
defer logfile.Close()
log.SetOutput(logfile)
log.Print("Logging to file")
}

// Using This method as it works for single and recursive
func cprf(src, dst string) {
	if _, err := os.Stat(src); err != nil {
    if os.IsNotExist(err) {
       log.Fatalf("Copying %s failed with %e: Path does not exist\n", src, err)
    }
	}
	_, err := exec.Command("cp", "-rf", src, dst).CombinedOutput()
	if err != nil {
			log.Fatalf("Copying "+src+" to "+dst+" failed with %s\n", err)
	}
	fmt.Println("Copying "+src+" to "+dst)
}

func create_dir(dir string) {
  if _, err := os.Stat(dir); os.IsNotExist(err) {
     err = os.MkdirAll(dir, 0755)
     if err != nil {
        panic(err)
     }
  }
}

func run_shell(sh_command string) string {
	 cmd := exec.Command("bash", "-c", sh_command)
   shell_out, err := cmd.CombinedOutput()
   if err != nil {
      log.Fatalf("cmd.Run() failed with %s\n", err)
   }
   return strings.TrimSuffix(string(shell_out), "\n")
}

//Core functions
func create_tmp_dir() string {
	Tmp_Dir, err := ioutil.TempDir(Tmp_Loc, "/golivecd-")
	if err != nil {
		log.Fatal(err)
	}
	return Tmp_Dir
}

func create_boot_dirs() {
	fmt.Println("Preparing Temp directory structure")
	create_dir(Tmp_Dir+"/live")
	create_dir(Tmp_Dir+"/live/EFI")
	create_dir(Tmp_Dir+"/live/LiveOS")
}

func create_isolinux() {
  fmt.Println("Copying files and configs needed for isolinux")
  create_dir(Tmp_Dir+"/live/isolinux")
  cprf(Root_Dir+"usr/share/syslinux/isolinux.bin", Tmp_Dir+"/live/isolinux/isolinux.bin")
  cprf(Root_Dir+"usr/share/syslinux/vesamenu.c32", Tmp_Dir+"/live/isolinux/vesamenu.c32")
  cprf(Root_Dir+"usr/share/syslinux/libcom32.c32", Tmp_Dir+"/live/isolinux/libcom32.c32")
  cprf(Root_Dir+"usr/share/syslinux/libutil.c32", Tmp_Dir+"/live/isolinux/libutil.c32")
  cprf(Root_Dir+"usr/share/syslinux/ldlinux.c32", Tmp_Dir+"/live/isolinux/ldlinux.c32")
  cprf(Source_Dir+"lib/modules/"+Latest_Kern+"/vmlinuz", Tmp_Dir+"/live/isolinux/vmlinuz0")
        // Copies 02livecd.tmpl from golivecd dir
	dracut_tmpl := (Source_Dir+"etc/dracut.conf.d/02livecd.conf")
	if _, err := os.Stat("02livecd.tmpl"); err != nil {
		 cprf(Root_Dir+"usr/share/golivecd/02livecd.tmpl", dracut_tmpl)
	} else {
		 cprf("02livecd.tmpl", dracut_tmpl)
	}
	if _, err := os.Stat(dracut_tmpl); err != nil {
		if os.IsNotExist(err) {
			 log.Fatalf("Reading %s failed with %e: Path does not exist\n", dracut_tmpl, err)
		}
	}
	parse_boot_cfg(dracut_tmpl)

	// Copies isolinux.tmpl from golivecd dir
	boot_tmpl := (Tmp_Dir+"/live/isolinux/isolinux.cfg")
	if _, err := os.Stat("isolinux.tmpl"); err != nil {
                 cprf(Root_Dir+"usr/share/golivecd/isolinux.tmpl", boot_tmpl)
	} else {
		 cprf("isolinux.tmpl", boot_tmpl)
	}
	if _, err := os.Stat(boot_tmpl); err != nil {
		if os.IsNotExist(err) {
			 log.Fatalf("Reading %s failed with %e: Path does not exist\n", boot_tmpl, err)
		}
	}
	parse_boot_cfg(boot_tmpl)

}

func create_grub2() {
  fmt.Println("Copying files and configs needed for grub2")
  create_dir(Tmp_Dir+"/live/grub2")
  cprf(Root_Dir+"usr/lib/grub/*", Tmp_Dir+"/live/grub2/")
  cprf(Root_Dir+"boot/grub2/themes", Tmp_Dir+"/live/grub2")
  cprf(Root_Dir+"usr/share/syslinux/libcom32.c32", Tmp_Dir+"/live/isolinux/libcom32.c32")
  cprf(Root_Dir+"usr/share/syslinux/libutil.c32", Tmp_Dir+"/live/isolinux/libutil.c32")
  cprf(Root_Dir+"usr/share/syslinux/ldlinux.c32", Tmp_Dir+"/live/isolinux/ldlinux.c32")
  cprf(Source_Dir+"lib/modules/"+Latest_Kern+"/vmlinuz", Tmp_Dir+"/live/isolinux/vmlinuz0")
}

func create_initrd() {
	fmt.Println("Creating init via dracut for kernel: "+Latest_Kern)
	var initrd_path string
	initrd_path = string(Tmp_Dir+"/live/isolinux/initrd0.img")
	if _, err := os.Stat(Tmp_Dir+"/live/isolinux"); err != nil {
		 if os.IsNotExist(err) {
			 initrd_path = string(Tmp_Dir+"/live/grub2/initrd0.img")
		   }
  }
	dracut_command := string("dracut --force --kver="+Latest_Kern+" --kmoddir="+Source_Dir+"lib/modules/"+Latest_Kern+" "+initrd_path+" "+Latest_Kern)
	do_dracut := run_shell(dracut_command)
	fmt.Println(do_dracut)
}

func livesys_setup() {
	if _, err := os.Stat(Source_Dir+"etc/rc.d/init.d/livesys"); err != nil {
	  if os.IsNotExist(err) {
             // Copies livesys.tmpl from golivecd dir
             livesys_tmpl := (Source_Dir+"etc/rc.d/init.d/livesys")
             if _, err := os.Stat("livesys.tmpl"); err != nil {
                    cprf(Root_Dir+"usr/share/golivecd/livesys.tmpl", livesys_tmpl)
             } else {
                    cprf("livesys.tmpl", livesys_tmpl)
             }
             if _, err := os.Stat(livesys_tmpl); err != nil {
                     if os.IsNotExist(err) {
                              log.Fatalf("Reading %s failed with %e: Path does not exist\n", livesys_tmpl, err)
                     }
             }
             parse_boot_cfg(livesys_tmpl)

             // Copies livesys-late.tmpl from golivecd dir
             livesys_late_tmpl := (Source_Dir+"etc/rc.d/init.d/livesys-late")
             if _, err := os.Stat("livesys-late.tmpl"); err != nil {
                      cprf(Root_Dir+"usr/share/golivecd/livesys-late.tmpl", livesys_late_tmpl)
             } else {
                      cprf("livesys-late.tmpl", livesys_late_tmpl)
             }
             if _, err := os.Stat(livesys_late_tmpl); err != nil {
                     if os.IsNotExist(err) {
                              log.Fatalf("Reading %s failed with %e: Path does not exist\n", livesys_late_tmpl, err)
                     }
             }
             parse_boot_cfg(livesys_late_tmpl)

           if Source_Dir != "/" {
              fmt.Println("Source Dir is not / assuming chroot.")
	   } else {
	      fmt.Println("Source Dir is / assuming local system.")
	   }

           if Source_Dir != "/" {
              mount_proc := string("mount -t proc proc "+Source_Dir+"/proc")
              mount_sys := string("mount -t sysfs sys "+Source_Dir+"/sys")
              mount_dev := string("mount -o bind /dev "+Source_Dir+"/dev")
              run_shell(mount_proc)
              run_shell(mount_sys)
              run_shell(mount_dev)
           }

           command_chmod := string("chroot "+Source_Dir+" /bin/bash -c \"chmod 755 /etc/rc.d/init.d/livesys*\"")
           command_restorecon := string("chroot "+Source_Dir+" /bin/bash -c \"/usr/bin/chcon -t initrc_exec_t /etc/rc.d/init.d/livesys*\"")
           command_chcon_pass := string("chroot "+Source_Dir+" /bin/bash -c \"/usr/bin/chcon -t passwd_file_t /etc/passwd\"")
           command_chcon_shad := string("chroot "+Source_Dir+" /bin/bash -c \"/usr/bin/chcon -t shadow_t /etc/shadow\"")
           command_chkconfig1 := string("chroot "+Source_Dir+" /bin/bash -c \"/sbin/chkconfig --add livesys\"")
           command_chkconfig2 := string("chroot "+Source_Dir+" /bin/bash -c \"/sbin/chkconfig --add livesys-late\"")

           run_shell(command_chmod)
           run_shell(command_restorecon)
           run_shell(command_chcon_pass)
           run_shell(command_chcon_shad)
           run_shell(command_chkconfig1)
           run_shell(command_chkconfig2)

           if SE_Stat != "Disabled" {
              fmt.Println("Setting SELinux Labels and Contexts: SELinux on Host enabled.")
	      setfile_cmd := string("/usr/sbin/setfiles /etc/selinux/targeted/contexts/files/file_contexts "+Source_Dir)
	      restorecon_cmd := string("/usr/sbin/restorecon -p -e /proc -e /sys -e /dev -F -R "+Source_Dir)
	      run_shell(restorecon_cmd)
	      run_shell(setfile_cmd)
	   }

	   if Source_Dir != "/" {
              unmount_proc := string("umount "+Source_Dir+"/proc")
              unmount_sys := string("umount "+Source_Dir+"/sys")
              unmount_dev := string("umount "+Source_Dir+"/dev")
              run_shell(unmount_dev)
	      run_shell(unmount_sys)
	      run_shell(unmount_proc)
           }

        }
      } else {
	      fmt.Println("Skipping livesys setup: livesys file found.")
      }
}

func do_squash(excludes string) {
	fmt.Println("Creating SquashFS image from: "+Source_Dir)
	create_dir(Tmp_Dir+"/live/LiveOS")
	squashfilepath := string(Tmp_Dir+"/live/LiveOS/squashfs.img")
	cmd := exec.Command("mksquashfs")
	cmd.Args = append(cmd.Args,
		Source_Dir, squashfilepath,
		"-noappend",
		"-comp", "xz",
		"-b", "1M",
		"-Xbcj", "x86",
		"-no-fragments",
		"-progress",
	)
	if len(excludes) > 0 {
		cmd.Args = append(cmd.Args, "-wildcards")
		cmd.Args = append(cmd.Args, "-e","'... *.pid'")
		cmd.Args = append(cmd.Args, "-e","'... *.log'")
		cmd.Args = append(cmd.Args, "-e","etc/fstab")
		s_exclude := strings.TrimSpace(excludes)
                exclude := strings.Split(s_exclude, " ")
		for i := range exclude {
		   fmt.Println("Excluding: "+exclude[i])
		   cmd.Args = append(cmd.Args, "-e", exclude[i])
		}
	}

        log.Println(cmd)

	var stdoutBuf, stderrBuf bytes.Buffer
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()

	var errStdout, errStderr error
	stdout := io.MultiWriter(os.Stdout, &stdoutBuf)
	stderr := io.MultiWriter(os.Stderr, &stderrBuf)
	err := cmd.Start()
	if err != nil {
			log.Fatalf("cmd.Start() failed with '%s'\n", err)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		_, errStdout = io.Copy(stdout, stdoutIn)
		wg.Done()
	}()

	_, errStderr = io.Copy(stderr, stderrIn)
	wg.Wait()

	err = cmd.Wait()
	if err != nil {
			log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	if errStdout != nil || errStderr != nil {
			log.Fatal("failed to capture stdout or stderr\n")
	}
}

func create_uefi_img() {
	fmt.Println("Creating UEFI Image")
	create_dir(Live_Dir+"/EFI")
	create_dir(Live_Dir+"/isolinux/")
	mkdosfs := string("mkdosfs -F12 -n "+ISO_Name+"_EFI -C "+Live_Dir+"/isolinux/efiboot.img 2048")
	mcopy := string("mcopy -s -i "+Live_Dir+"/isolinux/efiboot.img "+Live_Dir+"/EFI ::")
	run_shell(mkdosfs)
	run_shell(mcopy)
}

func create_iso() {
	oserr := os.Chdir(Tmp_Dir+"/live/")
	if oserr != nil {
			panic(oserr)
	}

	fmt.Println("Creating ISO from: "+Live_Dir)
	isofilepath := string(ISO_Dest+"/"+ISO_Name+".iso")
	fmt.Println("Attempting to write ISO to: "+isofilepath)
	cmd := exec.Command("xorrisofs")
	cmd.Args = append(cmd.Args,
		"-joliet", "-rational-rock",
		"-hide-rr-moved",
		"-volid", ISO_Name,
		"-output", isofilepath,
		"-isohybrid-mbr", "/usr/share/syslinux/isohdpfx.bin",
		"-eltorito-boot", "isolinux/isolinux.bin",
		"-eltorito-catalog", "isolinux/boot.cat",
		"-no-emul-boot", "-boot-info-table",
		"-boot-load-size", "4",
	)
	if _, err := os.Stat(Live_Dir+"/isolinux/efiboot.img"); err == nil {
		cmd.Args = append(cmd.Args,
			"-eltorito-alt-boot",
			"-e", "isolinux/efiboot.img",
			"-no-emul-boot",
			"-isohybrid-gpt-basdat", "-isohybrid-apm-hfsplus",
		)
	}
	if _, err := os.Stat(Live_Dir+"/isolinux/macboot.img"); err == nil {
		cmd.Args = append(cmd.Args,	
			"-eltorito-alt-boot",
			"-e", "isolinux/macboot.img",
			"-no-emul-boot",
			"-isohybrid-gpt-basdat", "-isohybrid-apm-hfsplus",
		)
	}
	cmd.Args = append(cmd.Args,Live_Dir)

	var stdoutBuf, stderrBuf bytes.Buffer
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()

	var errStdout, errStderr error
	stdout := io.MultiWriter(os.Stdout, &stdoutBuf)
	stderr := io.MultiWriter(os.Stderr, &stderrBuf)
	err := cmd.Start()
	if err != nil {
			log.Fatalf("cmd.Start() failed with '%s'\n", err)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		_, errStdout = io.Copy(stdout, stdoutIn)
		wg.Done()
	}()

	_, errStderr = io.Copy(stderr, stderrIn)
	wg.Wait()

	err = cmd.Wait()
	if err != nil {
			log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	if errStdout != nil || errStderr != nil {
			log.Fatal("failed to capture stdout or stderr\n")
	}
}

func cleanup() {
	if No_Clean ==false {
	 os.RemoveAll(Tmp_Dir)
	 os.Remove(Source_Dir+"etc/dracut.conf.d/02livecd.conf")
	 os.Remove(Source_Dir+"etc/rc.d/init.d/livesys")
	 os.Remove(Source_Dir+"etc/rc.d/init.d/livesys-late")
  }

}

func main() {

	  // Define Command line Args, defaults and description
		// Defaults, not required
		TmpLocPtr := flag.String("tmploc", "/tmp", "Temp Dir Location")
		Root_DirPtr := flag.String("rootdir", "/", "Root Dir Location")
		ISO_NamePtr := flag.String("isoname", "golivecd", "Name of the ISO Generated")
		ISO_DestPtr := flag.String("isodest", "/tmp", "Destination of the ISO Generated")
		Dist_NamePtr := flag.String("distname", "GOLiveCD", "Name of the Distribution (used in bootmenus)")
		Live_UserPtr := flag.String("liveuser", "liveuser", "Name of the User used for the live login")
		ExcludesPtr := flag.String("exclude", "", "List of Files or Folders to exclude")
		No_CleanPtr := flag.Bool("noclean", false, "Keep all files used for build")
		Force_SELinuxPtr := flag.Bool("selinuxon", false, "Force Enable SELinux if not available on Host")

                // No Default, required -srcdir
                Source_DirPtr := flag.String("srcdir", "", "Source Dir Location")

		//Attached to Global Var set via func
		kverPtr := flag.String("kver", "", "Kernel Version")
		flag.Parse()

                // Globals
		Root_Dir = (*Root_DirPtr)
		Source_Dir = (*Source_DirPtr)
		ISO_Name = (*ISO_NamePtr)
		ISO_Dest = (*ISO_DestPtr)
		Dist_Name = (*Dist_NamePtr)
		Tmp_Loc = (*TmpLocPtr)
		No_Clean = (*No_CleanPtr)
		Force_SELinux = (*Force_SELinuxPtr)
		Live_User = (*Live_UserPtr)
		Excludes = (*ExcludesPtr)
		Tmp_Dir = create_tmp_dir()
		Live_Dir = string(Tmp_Dir+"/live")
                exclu_tmp := Tmp_Dir[1:len(Tmp_Dir)]
		kver := (*kverPtr)

		start_logging()
                // If the kernel version is not defined assume latest found
                if kver == "" {
                   Latest_Kern = Latest_KVer()
                } else {
                   Latest_Kern = kver
                }

		// Define SE_Stat
		if Force_SELinux == false {
		   SE_Stat = selinux_stat() 
	           if strings.Contains(SE_Stat, "Disabled") {
		      fmt.Println("Disabling SELinux On Boot: SELinux is "+SE_Stat+" on Host Machine.")
		   } else {
		      fmt.Println("SELinux is "+SE_Stat)
		   }
		} else {
		   SE_Stat = string("Enabled")
		}

		// Run golivecd functions
	        if Source_Dir == "" {
			 Usage()
			 log.Fatalf("A Source Directory must be defined: ex. golivecd --srcdir=/chroot")
	        } else {
		fmt.Println("Using Kernel Version: "+Latest_Kern)
		user_check()
		create_boot_dirs()
		selinux_stat()
		create_isolinux()
		//create_grub2() at future addition
		create_initrd()
		livesys_setup()
		do_squash(exclu_tmp+" proc/* sys/* tmp/* mnt/* "+Excludes)
		create_uefi_img()
		create_iso()
		cleanup()
	  }
}
