%define debug_package %{nil}

Name:		golivecd
Version:	0.5.3
Release:	1%{?dist}
Summary:	Build ISOs using a chroot or root filesystem

License:	GPLv2
URL:		https://gitlab.com/unity-linux/golivecd
# Source gets created in gitlab build
Source0: golivecd-src.tar.xz

BuildRequires: %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}
Requires:   kernel-core, kernel-modules, dracut, dracut-live
Requires:   xorriso, syslinux, squashfs-tools, policycoreutils, mtools, dosfstools

%description
Build ISOs using a chroot or root filesystem

%prep
%setup -q -n %{name}

%build
mkdir bin
%gobuild -o bin/golivecd ./...

%install
install -d %{buildroot}/%{_bindir}
install -d %{buildroot}/%{_datadir}/%{name}
install -p -m 755 bin/%{name} %{buildroot}%{_bindir}
install -p -m 0644 *.tmpl %{buildroot}%{_datadir}/%{name}/

%files
%doc README.md
%{_bindir}/%{name}
%dir %attr(755,root,root) %{_datadir}/%{name}
%{_datadir}/%{name}/*.tmpl

%changelog
