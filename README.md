# golivecd

This readme is super incomplete and this program has not been well tested for all practical uses.. sorry

GoLiveCD is a simple program written in GoLang that takes a /(root) or chroot, compresses the tree using mksquashfs to create a disk image and then uses xorrisofs to create an ISO based on information obtained by the below flags.

```
  --distname string        Name of the Distribution (used in bootmenus) (default "GOLiveCD")
  --isodest string         Destination of the ISO Generated (default "/tmp")
  --isoname string         Name of the ISO Generated (default "golivecd")
  --kver string            Kernel Version
  --noclean                Keep all files used for build
  --rootdir string         Root Dir Location (default "/")
  --srcdir string          Source Dir Location
  --tmploc string          Temp Dir Location (default "/tmp")
  --exclude string         Exclude files or directories (default "proc sys tmp mnt")
```

Currently the only required field is the Source Directory location. So an example command would be.

```
sudo golivecd --srcdir=/chroot
```
by default golivecd writes generation files and the final iso to tmp. On some machines /tmp is limited to a ramfs size of ~4G. IF this is the case for you, specify a different tmp location by adding the --tmploc flag.
```
sudo golivecd --srcdir=/chroot --tmploc=/home/<your username>
```
After running the above command golivecd will attempt to create a bootable Live ISO. This is assuming that all the needed files and setup has been done in the source directory it's been pointed too.
